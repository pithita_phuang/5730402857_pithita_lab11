<?php

if(isset($_GET['alt']) && ($_GET['alt']=="xml")){
    
    $stu_xml = new SimpleXMLElement("<students></students>");
    
    $student = $stu_xml->addChild('student');
    $student-> addChild('names', 'Pithita Phuangsuwan');
    $books = $student-> addChild('books');
    $books -> addChild('book', 'Fifty shades of Grey');
    $books -> addChild('book', 'Keroro');
    $books -> addChild('book', 'Hello Kitty');
    $education = $stu_xml->addChild('education');
    $education->addChild('high school','Udonpittayanukool');
    $education->addChild('undergraduated school', 'KhonKaen University');
    
    $student= $stu_xml->addChild('student');
    $student -> addChild('names', 'ชนพัฒน์');
    $books = $student-> addChild('books');
    $books -> addChild('book', 'ภาพปริศนา');
    $books -> addChild('book', 'พระพุทธเจ้า');
    $books -> addChild('book', 'หมากกระดาน');
    $education = $stu_xml-> addChild('education');
    $education->addChild('high school','Satit KKU');
    $education->addChild('undergraduated school', 'KhonKaen University');
    
    Header('Content-Type: text/xml');
    echo $stu_xml->asXML();

}
else{

$data = array('student' =>
        array(
            array('name' => 'Pithita Phuangsuwan',
                'books' => array('Fifty shades of Grey', 'Keroro', 'Hello Kitty'),
                'education' => array(
                    array('highschool' => 'Udonpittayanukool')
                    ,
                    array('undergradschool' => 'Khon Kaen University')
                )
            ),
            array('name' => 'ชนพัฒน์',
                'books' => array('ภาพปริศนา', 'พระพุทธเจ้า', 'หมากกระดาน'),
                'education' => array(
                    array('highschool' => 'Satit KKU')
                    ,
                    array('undergradschool' => 'KhonKaen University')
                )
            )
        )
    );

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    echo $json;
}

?>
