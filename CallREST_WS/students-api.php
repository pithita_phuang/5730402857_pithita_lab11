<?php

if(isset($_GET['alt']) && ($_GET['alt']=="xml")){
    
    $stu_xml = new SimpleXMLElement("<students></students>");
    
    $student = $stu_xml->addChild('student');
    $student-> addChild('name', 'Pithita Phuangsuwan');
    $books = $student-> addChild('books');
    $books -> addChild('book', 'Fifty shades of Grey');
    $books -> addChild('book', 'Keroro');
    $books -> addChild('book', 'Hello Kitty');
    $education = $student -> addChild('education');
    $education -> addChild('highSchool', 'Udonpittayanukool');
    $education -> addChild('undergradSchool', 'KhonKaen University');
    
    $student = $stu_xml->addChild('student');
    $student -> addChild('name', 'ชนพัฒน์');
    $books = $student-> addChild('books');
    $books -> addChild('book', 'ภาพปริศนา');
    $books -> addChild('book', 'พระพุทธเจ้า');
    $books -> addChild('book', 'หมากกระดาน');
    $education = $student-> addChild('education');
    $education -> addChild('highSchool','Satit KKU');
    $education -> addChild('undergradSchool', 'KhonKaen University');
    
    Header('Content-Type: text/xml');
    echo $stu_xml->asXML();

}
else{

$data = array('students' =>
        array(
            array('name' => 'Pithita Phuangsuwan',
                'books' => array('Fifty shades of Grey', 'Keroro', 'Hello Kitty'),
                'education' => array(
                    array('high school' => 'Udonpittayanukool')
                    ,
                    array('undergrad school' => 'Khon Kaen University')
                )
            ),
            array('name' => 'ชนพัฒน์',
                'books' => array('ภาพปริศนา', 'พระพุทธเจ้า', 'หมากกระดาน'),
                'education' => array(
                    array('high school' => 'Satit KKU')
                    ,
                    array('undergrad school' => 'KhonKaen University')
                )
            )
        )
    );

    $json = json_encode($data, JSON_UNESCAPED_UNICODE);
    echo $json;
}

?>
